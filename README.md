# BA

This repository contains all code relevant to my bachelor's thesis:

**Image Reconstruction based on a Sinogram using a Physics-informed Neural
Network**.

Implementation is done in Python.
* `image_prep.py` creates and prepares images for the method testing,
* `main.py` is a command-line interface for testing the PINN-inspired method,
* `reconstructor.py` contains the class which handles PINN-inspired image
reconstruction.


## Images
Images `cosine.png`, `squares.png`, and `camera.png` used in test runs:

![cosine.png](images/cosine.png)
![squares.png](images/squares.png)
![camera.png](images/camera.png)




## Required Packages and Versions
I recommend to create a virtual environment and install the following packages:
```
# Name                    Version                   Build  Channel
matplotlib                3.7.1            py39h06a4308_1  
numpy                     1.23.5           py39h14f4228_0  
python                    3.9.16               h7a1cb2a_0  
scikit-image              0.19.3           py39h6a678d5_1  
tensorflow                2.4.1           gpu_py39h8236f22_0  
```


## Usage
The testing is done with the command-line interface `main.py`.
```
$ python main.py --help
```
prints the following usage help:
```
usage: python main.py [-h] [-a N_ANGLES] [-l N_HIDDEN] [-n N_NEURONS]
                      [-r LEARN_RATE] [-e N_EPOCHS] [-b BATCHSIZE]
                      {cosine,squares,camera,camera_64,camera_256}

Test the PINN-inspired image reconstruction method.

positional arguments:
  {cosine,squares,camera,camera_64,camera_256}
                        grayscale image to be used

optional arguments:
  -h, --help            show this help message and exit
  -a N_ANGLES, --n_angles N_ANGLES
                        number of projection angles in [0,pi) (default: 180)
  -l N_HIDDEN, --n_hidden N_HIDDEN
                        number of hidden layers of the model (default: 4)
  -n N_NEURONS, --n_neurons N_NEURONS
                        number of neurons per hidden layer (default: 40)
  -r LEARN_RATE, --learn_rate LEARN_RATE
                        initial learning rate (default: 0.001)
  -e N_EPOCHS, --n_epochs N_EPOCHS
                        number of epochs of training (default: 10)
  -b BATCHSIZE, --batchsize BATCHSIZE
                        batchsize of training data (default: 16)
```
