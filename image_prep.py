import numpy as np
import skimage as si
import matplotlib.pyplot as plt


IMG_SHAPE = (128,128)


def saveImage(directory, image_name, image):
    """
    Saves an image as .png and as .txt (numpy array) to preserve floating point
    grayscale-values in [0,1].
    """
    plt.imsave(f"{directory}/{image_name}.png", image, cmap=plt.cm.gray)
    np.savetxt(f"{directory}/{image_name}.txt", image)


def main():
    """ Creates and prepares all images for testing. """
    # COSINE
    cos2   = lambda x, y: (np.cos(np.sqrt((x/4)**2 + (y/4)**2)) + 1)/2
    cosine = np.zeros(IMG_SHAPE)
    for (x,y), val in np.ndenumerate(cosine):
        cosine[x,y] = cos2(x - 64, y - 64)
    saveImage("images", "cosine", cosine)

    # SQUARES
    squares = np.zeros(IMG_SHAPE)
    gap = IMG_SHAPE[0] // 4
    squares[:,:] = 0.5
    squares[gap:3*gap, gap:3*gap] = 1.
    squares[gap:2*gap, gap:2*gap] = 0.
    saveImage("images", "squares", squares)

    # CAMERA
    camera = si.data.camera()
    camera_64  = si.transform.resize(camera,   (64,64), anti_aliasing=True)
    camera_256 = si.transform.resize(camera, (256,256), anti_aliasing=True)
    camera     = si.transform.resize(camera, IMG_SHAPE, anti_aliasing=True)
    saveImage("images", "camera",     camera)
    saveImage("images", "camera_64",  camera_64)
    saveImage("images", "camera_256", camera_256)

    for image in [cosine, squares, camera]:
        if image.max() > 1 or image.min() < 0:
            raise ValueError("Grayscale-values of image not in [0,1].")


if __name__ == "__main__":
    main()
