import time
import numpy as np
import tensorflow as tf


class Reconstructor:
    """
    Class to reconstruct a (grayscale & square) image from a sinogram by
    training a neural network model.
    """
    def __init__(self, sinogram, n_hidden, n_neurons):
        self.sinogram   = sinogram
        self.n_pnts     = sinogram.shape[0] # u
        self.n_rots     = sinogram.shape[1] # k
        self.img_width  = int(self.n_pnts/np.sqrt(2))
        self.center     = np.array([self.n_pnts, self.n_pnts])/2 \
                        - np.array([0.5, 0.5])
        self.pad        = (self.n_pnts - self.img_width)//2
        self.origin     = np.array([self.pad, self.pad])
        self.loss       = tf.keras.losses.MeanSquaredError()

        self.model      = tf.keras.models.Sequential([
            tf.keras.Input(shape=(None, 2), name="coordinates"),    # input
            *[tf.keras.layers.Dense(n_neurons, activation="relu")   # hidden
                for _ in range(n_hidden)],
            tf.keras.layers.Dense(1,                                # output
                                  activation="sigmoid",
                                  name="grayscale-value")])


    #---------- PRIVATE ----------#

    def rotate(self, points, phi):
        """ Rotates a 2D-point (row,col) or (x,y) by phi around (0,0). """
        # Rotation Matrix
        R = np.array([[np.cos(phi), -np.sin(phi)],
                      [np.sin(phi),  np.cos(phi)]])
        return np.matmul(R, points.T).T # transpose for correct matmul

    def getLine(self, u, k):
        """
        Form a line of n_pnts normalized points (row,col) in original image
        representing model training input.

        Positional arguments:
        u -- row of sinogram
        k -- col of sinogram (representing the k'th angle phi of rotation)
        """
        phi = k*np.pi/self.n_rots

        # Create vertical line at column k in padded image
        line = np.zeros((self.n_pnts, 2))
        line[:,0] = np.linspace(0, self.n_pnts, self.n_pnts, endpoint=False)
        line[:,1] = u

        # Get new origin of image f inside f_pad image by rotating around center
        origin = self.rotate(self.origin - self.center, -phi) + self.center
        # Transform each line point to new origin and scale down by img_width
        line = self.rotate(line - origin, phi)/self.img_width

        # Set all out of bounds coords to (0,0) (out of bounds when not in [0,1])
        condition = np.logical_or(line < 0, line > 1).any(axis=1)
        line = np.where(np.vstack([condition,condition]).T, 0., line)
        return line

    def getAllData(self):
        """
        Returns all lines over which the image was projected in sinogram
        creation and their corresponding sum (integral) value.
        """
        n_lines = self.sinogram.size
        lines   = np.zeros((n_lines, self.n_pnts, 2))
        sums    = np.zeros(n_lines)
        for (u, k), sum in np.ndenumerate(self.sinogram):
            iter = k*u + u
            lines[iter, :, :] = self.getLine(u, k)
            sums [iter]       = sum
        return lines, sums

    @tf.function()
    def trainStep(self, line, trueSum):
        with tf.GradientTape() as tape:
            pred    = self.model(line, training=True)
            predSum = tf.math.reduce_sum(pred, axis=-2) # sum up lines
            loss_value   = self.loss(predSum, trueSum)
        grads = tape.gradient(loss_value, self.model.trainable_weights)
        self.optimizer.apply_gradients(zip(grads, self.model.trainable_weights))
        return loss_value

    def runEpoch(self, batchsize, lines, sums):
        """ Trains the model for one epoch. """
        epoch_loss = 0
        for i in np.arange(sums.size, step=batchsize):
            lineBatch   = lines[i:i+batchsize, :,  :]
            sumBatch    =  sums[i:i+batchsize]
            loss_value  = self.trainStep(lineBatch, sumBatch)
            epoch_loss += loss_value
        return epoch_loss


    #---------- PUBLIC ----------#

    def train(self, n_epochs, lr, batchsize):
        """ Performs the model training to learn the image. """
        self.optimizer  = tf.keras.optimizers.Adam(learning_rate=lr)
        lines, sums = self.getAllData()
        seconds_sum = 0
        for epoch in range(1, n_epochs + 1):

            # Randomize order of lines and sums the same way (same seed)
            a = np.random.default_rng(seed=42)
            b = np.random.default_rng(seed=42)
            a.shuffle(lines)
            b.shuffle(sums)

            start = time.time()
            epoch_loss = self.runEpoch(batchsize, lines, sums)
            end = time.time()
            seconds   = end - start
            seconds_sum += seconds
            print(f"Epoch {epoch}/{n_epochs}:",
                  f"loss = {epoch_loss:10.0f},   took {seconds:3.2f}s")
        seconds_avg = seconds_sum/n_epochs
        print(f"Average epoch took {seconds_avg:3.2f}s")
        return seconds_avg

    def createImage(self):
        """
        Creates an image array and fills it's pixels with grayscale values given
        by the model.
        """
        image = np.zeros((self.img_width, self.img_width))
        for (row, col), val in np.ndenumerate(image):
            image[row, col] = self.model(np.array([[row/self.img_width,
                                                    col/self.img_width]]))
        return image
