import os
import argparse
import numpy as np
import skimage as si
import matplotlib.pyplot as plt
from scipy.fft import fft2
from datetime import datetime

from reconstructor import Reconstructor
from image_prep import saveImage


def getPSNR(image, recon):
    """
    Computes the peak signal to-noise-ratio (PSNR) of a reconstructed image.
    """
    mse = ((recon - image)**2).mean()
    return 10*np.log10(image.max()**2/mse)


def getSSIM(image, recon):
    """
    Computes the structure similarity index measure (SSIM) of a reconstructed image.
    """
    return si.metrics.structural_similarity(
        image, recon, data_range=recon.max() - recon.min())


def doExperiment(image_name,
                 n_angles,
                 n_hidden,
                 n_neurons,
                 learn_rate,
                 n_epochs,
                 batchsize):
    """
    Perform the experiment and write results into the log directory.
    """

    # Create the log directory, which will contain the results
    dt     = datetime.now().strftime("%Y-%m-%d--%H-%M-%S")
    logdir = "logs/" + dt
    os.mkdir(logdir)

    # Print info string
    infostring = f"""
image_name  = {image_name}
n_angles    = {n_angles}
n_hidden    = {n_hidden}
n_neurons   = {n_neurons}
learn_rate  = {learn_rate}
epochs      = {n_epochs}
batchsize   = {batchsize}
datetime    = {dt}"""
    print(infostring)

    # Load image
    image    = np.loadtxt(f"images/{image_name}.txt")

    # Compute sinogram
    theta    = np.linspace(0., 180., n_angles, endpoint=False)
    sinogram = si.transform.radon(image, theta=theta, circle=False)
    saveImage(logdir, "sinogram", sinogram)

    # Perform FBP (skimage: "iradon") for comparison
    recon_fbp = si.transform.iradon(sinogram, theta=theta, circle=False)
    d1 = (recon_fbp.shape[0] - image.shape[0])//2
    d2 = (recon_fbp.shape[1] - image.shape[1])//2
    recon_fbp = recon_fbp[d1:d1+image.shape[0], d2:d2+image.shape[1]]
    ssim_fbp  = getSSIM(image, recon_fbp)
    psnr_fbp  = getPSNR(image, recon_fbp)
    saveImage(logdir, f"recon_fbp", recon_fbp)

    # Perform PINN-inspired reconstruction
    reconstructor = Reconstructor(sinogram, n_hidden, n_neurons)
    seconds_avg = reconstructor.train(n_epochs, learn_rate, batchsize)
    recon = reconstructor.createImage()
    ssim  = getSSIM(image, recon)
    psnr  = getPSNR(image, recon)
    saveImage(logdir, f"recon", recon)

    # Do 2D fourier transform on image, recon, recon_fbp and save as image
    fft = lambda x: np.log(np.abs(np.fft.fftshift(np.fft.fft2(x))))
    fourier       = fft(image)
    fourier_recon = fft(recon)
    fourier_fbp   = fft(recon_fbp)
    saveImage(logdir, f"fourier",       fourier)
    saveImage(logdir, f"fourier_recon", fourier_recon)
    saveImage(logdir, f"fourier_fbp",   fourier_fbp)

    # Add seconds_avg, PSNRs and SSIMs to infostring and write to log
    a = f"\nseconds     = {seconds_avg}"
    b = f"\nPSNR        = {psnr}\nSSIM        = {ssim}"
    c = f"\nPSNR_FBP    = {psnr_fbp}\nSSIM_FBP    = {ssim_fbp}"
    print(a,b,c)
    with open(logdir + "/info.txt", 'w') as f:
        f.write(infostring + a + b + c)


def main():
    """ Parses the arguments and launches the experiment. """

    parser = argparse.ArgumentParser(
        prog="python main.py",
        description="Test the PINN-inspired image reconstruction method.")

    parser.add_argument(
        "image_name",
        choices=["cosine", "squares", "camera", "camera_64", "camera_256"],
        help="grayscale image to be used")

    parser.add_argument("-a", "--n_angles",     type=int,   default=180,
        help="number of projection angles in [0,pi) (default: %(default)s)")

    parser.add_argument("-l", "--n_hidden",     type=int,   default=4,
        help="number of hidden layers of the model  (default: %(default)s)")

    parser.add_argument("-n", "--n_neurons",    type=int,   default=40,
        help="number of neurons per hidden layer    (default: %(default)s)")

    parser.add_argument("-r", "--learn_rate",   type=float, default=1e-3,
        help="initial learning rate                 (default: %(default)s)")

    parser.add_argument("-e", "--n_epochs",     type=int,   default=10,
        help="number of epochs of training          (default: %(default)s)")

    parser.add_argument("-b", "--batchsize",    type=int,   default=16,
        help="batchsize of training data            (default: %(default)s)")

    args = parser.parse_args()
    doExperiment(args.image_name,
                 args.n_angles,
                 args.n_hidden,
                 args.n_neurons,
                 args.learn_rate,
                 args.n_epochs,
                 args.batchsize)


if __name__ == "__main__":
    main()
