import numpy as np
import skimage as si
import matplotlib
import matplotlib.pyplot as plt

AXIS = 0

def getPaddedImg(img):
    diag       = np.sqrt(2) * max(img.shape)
    pad        = [int(np.ceil((diag - s)/2)) for s in img.shape]
    return np.pad(img, (pad,pad), mode='constant', constant_values=0)

def getRotatedImg(img, phi):
    # clockwise
    return si.transform.rotate(img.copy(), -phi, resize=False)

def getSinogram(img, n_rots):
    img_pad  = getPaddedImg(img)
    sinogram = np.zeros((img_pad.shape[0], n_rots))
    for k in range(n_rots):
        img_pad_rot = getRotatedImg(img_pad.copy(), k*180/n_rots)
        sinogram[:, k] = img_pad_rot.sum(axis=AXIS)
    return sinogram


def main():
    camera         = np.loadtxt("images/camera.txt")
    camera_pad     = getPaddedImg(camera)
    camera_pad_rot = getRotatedImg(camera_pad, phi=45)
    summe          = camera_pad_rot.sum(axis=AXIS)
    summe = np.vstack([summe for _ in range(10)])

    fig, axs = plt.subplots(2, 2)
    axs[0,0].imshow(camera_pad_rot, cmap="gray")
    axs[1,0].imshow(summe, cmap="gray")

    # SINOGRAMS
    theta = np.linspace(0.,180.,180, endpoint=False)
    axs[0,1].imshow(si.transform.radon(camera, theta=theta, circle=False), cmap="gray")
    axs[1,1].imshow(getSinogram(camera, n_rots=180), cmap="gray")
    plt.show()


if __name__ == "__main__":
    main()
